//
//  DurationCalculatorTests.swift
//  TimeSegmentsTests
//
//  Created by Mohamed Serag on 3/31/20.
//  Copyright © 2020 Mohamed Serag. All rights reserved.
//

import XCTest
@testable import TimeSegments

class DurationCalculationTests: XCTestCase {
    var durationCalculator: DurationCalculator!
    
    func test_activeDuration() {
        // Given
        let expectation = self.expectation(description: "exp")
        let startDate = Date()
        var duration: DateComponents!
        
        // When
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { self.durationCalculator.pause() }
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) { self.durationCalculator.resume() }
        DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
            duration = self.durationCalculator.activeDuration(between: startDate, and: Date())
            expectation.fulfill()
        }
        
        // Then
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssertTrue([3,4,5].contains(duration.second!))
    }
    
    override func setUp() {
        durationCalculator = DurationCalculator()
    }
    
    override func tearDown() {
        durationCalculator = nil
    }
    
}

