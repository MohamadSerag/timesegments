//
//  TimeSegmentsTests.swift
//  TimeSegmentsTests
//
//  Created by Mohamed Serag on 3/28/20.
//  Copyright © 2020 Mohamed Serag. All rights reserved.
//

import XCTest
@testable import TimeSegments

class SegmentTimerTests: XCTestCase {
    var segmentTimer: SegmentTimer!
    
    override func setUp() {
        super.setUp()
        segmentTimer = SegmentTimer(tickInterval: 1)
    }
    
    override func tearDown() {
        _ = segmentTimer.end()
        segmentTimer = nil
        super.tearDown()
    }

    func testStart_subscription() {
        // Given
        let expectation = self.expectation(description: "start: subscriber to elapsed time gets updates")
        var elapsedTimes = [DateComponents?]()
        let sub = segmentTimer.$activeDuration.compactMap { $0 }.sink { elapsedTimes.append($0) }
        
        // When
        segmentTimer.start()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            sub.cancel()
            expectation.fulfill()
        }
        
        // Then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(elapsedTimes.count, 3)
    }

    func testStart_elapsedTime() {
        // Given
        let expectation = self.expectation(description: "start: tick")
        
        // When
        segmentTimer.start()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { expectation.fulfill() }
        
        // Then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(segmentTimer.activeDuration!.second!, 3)
    }

    func testElapsedTimeWhenAppChangesMode() {
        // Given
        let expectation = self.expectation(description: "elapsedTime: continuous counting on entering background")
        let nc = NotificationCenter.default
        
        // When
        segmentTimer.start()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { nc.post(name: UIApplication.willResignActiveNotification, object: nil) }
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) { nc.post(name: UIApplication.willEnterForegroundNotification, object: nil) }
        DispatchQueue.main.asyncAfter(deadline: .now() + 6) { expectation.fulfill() }
        
        // Then
        waitForExpectations(timeout: 8, handler: nil)
        XCTAssertEqual(segmentTimer.activeDuration!.second!, 6)
    }

    func testResume_pauseTimeSubtraction() {
        // Given
        let expectation = self.expectation(description: "resume: subtracting pause time from seg. duration")
        let durationSub = segmentTimer.$activeDuration.sink { _ in }
        // When
        segmentTimer.start()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { self.segmentTimer.pause() }
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) { self.segmentTimer.resume() }
        DispatchQueue.main.asyncAfter(deadline: .now() + 6) { self.segmentTimer.end() }
        DispatchQueue.main.asyncAfter(deadline: .now() + 7) { expectation.fulfill() }
        durationSub.cancel()
        
        // Then
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssertEqual(segmentTimer.activeDuration!.second!, 4)
    }
    
    func testPause_stoppingTimer() {
        // Given
        let expectation = self.expectation(description: "pause: stopping timer")
        let durationSub = segmentTimer.$activeDuration.sink { _ in }
        
        // When
        segmentTimer.start()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { self.segmentTimer.pause() }
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { expectation.fulfill() }
        durationSub.cancel()

        // Then
        waitForExpectations(timeout: 6, handler: nil)
        // if timer sub. wasn't canceled, every second elapsedTime prop. would get updated. second would be 5 not 3
        XCTAssertEqual(segmentTimer.activeDuration!.second!, 3)
    }

}
