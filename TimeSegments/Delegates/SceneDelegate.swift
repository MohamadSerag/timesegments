import UIKit
import SwiftUI

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        UITableView.appearance().separatorStyle = .none
        UITableView.appearance().backgroundColor = Color.uiSceneBackground
        
        let contentView = TabsView().environment(\.managedObjectContext, CoreDataStoreManager.shared.viewContext)
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = UIHostingController(rootView: contentView)
            self.window = window
            window.makeKeyAndVisible()
        }
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        CoreDataStoreManager.shared.saveChanges()
    }
}

