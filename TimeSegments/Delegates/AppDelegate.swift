import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        CoreDataStoreManager.shared.loadDefaults()
        NotificationManager.shared.removeNotifications() // in case the app crashed and didn't remove scheduled notification
        if UserDefaults.standard.opacity == 0 && !UserDefaults.standard.didRunBefore {
            UserDefaults.standard.opacity = 1
        }
        return true
    }
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        CoreDataStoreManager.shared.saveChanges()
        NotificationManager.shared.removeNotifications()
    }
}

//MARK:- Helpful Extensions
extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

extension UserDefaults {
    var didRunBefore: Bool {
        get { bool(forKey: "DidRunBefore") }
        set { set(true, forKey: "DidRunBefore")}
    }
}

