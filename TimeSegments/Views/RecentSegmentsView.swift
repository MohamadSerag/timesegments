import SwiftUI

struct RecentlyView<ViewModel: RecentlyViewModeling>: View {
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        List {
            ForEach(viewModel.cellViewModels, id: \.startTime) { vm in
                SegmentCell(viewModel: vm)
                    .padding(.vertical)
            }
            .onDelete(perform: viewModel.delete(at:))
            .listRowBackground(Color("platter-bg"))
        }
    }
}

struct RecentSegmentsView_Previews: PreviewProvider {
    static var previews: some View {
        RecentlyView(viewModel: RecentlyViewModel())
    }
}
