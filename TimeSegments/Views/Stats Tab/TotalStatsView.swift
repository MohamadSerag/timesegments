import SwiftUI

struct TotalStatsView<ViewModel: TotalStatsViewModeling>: View {
    @ObservedObject var vm: ViewModel
    var calc: StatsCalculating = CalcFactory()
    private let dateFooterText = "You can't select a date before your oldest recorded segment, or after your latest."
    
    var body: some View {
        GeometryReader { gr in
            VStack {
                if self.vm.periodSegments.count > 0 {
                    PieChart(chef: PieChef(slices: self.calc.chartSlices(from: self.vm.periodSegments, includeNoRecords: self.vm.includeNoRecords)))
                        .frame(width: gr.size.width, height: gr.size.height / 1.7)
                }
                else {
                    PlaceholderView(headline: "Need More Data!", subHeadline: "Record some segment to create the chart.")
                        .frame(width: gr.size.width, height: gr.size.height / 1.7)
                }
                
                Form {
                    Section(header: Text("Period"), footer: Text(self.dateFooterText)) {
                        DatePicker("Start Date", selection: self.$vm.startDate, in: self.vm.dateRange, displayedComponents: [.date])
                        DatePicker("End Date", selection: self.$vm.endDate, in: self.vm.dateRange, displayedComponents: [.date])
                    }
                }
            }
        }
    }
}
