import SwiftUI

struct SegmentStatsView<ViewModel: SegmentStatsViewModeling>: View {
    @ObservedObject var vm: ViewModel
    
    var body: some View {
        NavigationView {
            GeometryReader { gr in
                VStack(spacing: 0) {
                    List {
                        ForEach(self.vm.barInfoList) {
                            SegmentBarCell(info: $0)
                        }
                    }
                    .padding(.top)
                    .environment(\.defaultMinListRowHeight, 35)
                    .frame(height: gr.size.height / 2)
                    
                    HStack {
                        ZStack {
                            RoundedRectangle(cornerRadius: 5)
                                .fill(Color.blue)
                            Text(self.vm.totalDuration)
                                .bold()
                                .foregroundColor(.white)
                        }
                        .frame(height: 40)
                        .padding(.horizontal)
                    }
                    .padding(.vertical, 5)
                    .background(Color.sceneBackground)
                    
                    Form {
                        Section(header: Text("Period")) {
                            DatePicker("Start Date", selection: self.$vm.startDate, in: self.vm.startDateRange, displayedComponents: .date)
                            DatePicker("End Date", selection: self.$vm.endDate, in: self.vm.endDateRange, displayedComponents: .date)
                        }
                        Section {
                            Picker("Selected Segment", selection: self.$vm.selectedTitleIndex) {
                                ForEach(0..<self.vm.previousTitles.count, id: \.self) {
                                    Text(self.vm.previousTitles[$0]).tag($0)
                                }
                            }
                        }
                    }
                }
                .navigationBarTitle("", displayMode: .inline)
                .navigationBarHidden(true)
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}
