import SwiftUI

struct SegmentBarCell: View {
    let info: BarCellInfo
    private var formattedDuration: String {
        var minuteStr = "00"
        if info.minutes > 0 && info.minutes < 10 {
            minuteStr = "0\(info.minutes.description)"
        }
        else if info.minutes > 10 {
            minuteStr = info.minutes.description
        }
        return "\(info.hours.description):\(minuteStr)"
    }

    var body: some View {
        GeometryReader { gr in
            HStack(spacing: 5) {
                Text(self.info.dayName)
                    .font(.footnote)
                    .frame(width: 30)
                
                RoundedRectangle(cornerRadius: 5)
                    .foregroundColor(self.info.hasNoDuration ? .red : Color.green.opacity(0.8))
                    .frame(width: self.width(from: gr.size.width, using: CGFloat(self.info.lengthPercent)))
                
                Text(self.formattedDuration)
                    .font(.footnote)
                
                Spacer()
            }
            .foregroundColor(self.info.hasNoDuration ? .red : .primary)
        }
    }

    private func width(from availableWidth: CGFloat, using percentage: CGFloat) -> CGFloat {
        let barWidth = availableWidth - 82
        return percentage == 0 ? barWidth * 0.01 : barWidth * percentage
    }
}

struct BarCellInfo: Identifiable {
    let id = UUID()
    let dayName: String
    let lengthPercent: Double
    let hours: Int
    let minutes: Int
    var hasNoDuration: Bool { hours == 0 && minutes == 0 }
    
    init(dayName: String, lengthPercent: Double, hours: Int, minutes: Int) {
        self.dayName = dayName
        self.lengthPercent = lengthPercent
        self.hours = hours
        self.minutes = minutes
    }
    
    init(segment: SegmentEntity, lengthPercent: Double) {
        self.dayName = Calendar.current.dayName(for: segment.startDate!)
        self.lengthPercent = lengthPercent
        self.hours = segment.activeHours!
        self.minutes = segment.activeMinutes!
    }
}
