import XCTest
@testable import TimeSegments
import Combine

class TimerViewModelTest: XCTestCase {
    var viewModel: TimerViewModel!
    
    override func setUp() {
        super.setUp()
        viewModel = TimerViewModel(timer: SegmentTimer(tickInterval: 1))
    }
    
    override func tearDown() {
        viewModel = nil
        super.tearDown()
    }
    
    func test_init_dateSubscribersServed() {
        // Given
        var startDates = [String]()
        var subs = Set<AnyCancellable>()
        let exp = expectation(description: "exp")
        
        // When
        viewModel.objectWillChange.sink { startDates.append(self.viewModel.startTime) }.store(in: &subs)
        
        viewModel.startButtonTapped()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { exp.fulfill() }
        viewModel.endButtonTapped()
        
        // Then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(startDates.count, 2)
    }
    
    func test_init_durationSubscriberServed() {
        // Given
        let exp = expectation(description: "exp")
        var durations = [String]()
        let sub = viewModel.objectWillChange.sink { durations.append(self.viewModel.activeDurationMajor) }

        // When
        viewModel.startButtonTapped()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
            self.viewModel.endButtonTapped()
            sub.cancel()
            exp.fulfill()
        }

        // Then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(durations.count, 3)
    }
    
    func test_changingAppModeHandled() {
        let exp = expectation(description: "exp")
        var durations = [String]()
        let sub = viewModel.objectWillChange.sink { durations.append(self.viewModel.activeDurationMajor) }

        // When
        viewModel.startButtonTapped()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            NotificationCenter.default.post(name: UIApplication.willResignActiveNotification, object: nil)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            NotificationCenter.default.post(name: UIApplication.willEnterForegroundNotification, object: nil)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            self.viewModel.endButtonTapped()
            sub.cancel()
            exp.fulfill()
        }
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssertEqual(viewModel.activeDurationMinor, "04")
    }
    
}
