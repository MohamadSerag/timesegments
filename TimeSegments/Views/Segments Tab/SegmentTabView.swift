import SwiftUI

struct SegmentTabView: View {
    let exporter: SegmentExporting = SegmentExporter()
    @FetchRequest(fetchRequest: SegmentEntity.sortedFetchReq) var segments: FetchedResults<SegmentEntity>
    @State private var selectedIndex = 0
    @State private var sheetIndex: Int?
    
    var body: some View {
        VStack(spacing: 0) {
            HStack {
                Button(action: {
                    self.sheetIndex = 1
                }) {
                    Image(systemName: "square.and.arrow.up").imageScale(.large)
                }
                Spacer()
                Button(action: {
                    self.sheetIndex = 0
                }) {
                    Image(systemName: "plus.circle.fill").imageScale(.large)
                }
            }
            .padding()
            
            HStack {
                Text("Segments")
                    .font(.largeTitle)
                    .bold()
                    .padding(.leading)
                Spacer()
            }
            
            Picker(selection: $selectedIndex, label: Text("View Type")) {
                Text("Days").tag(0)
                Text("Months").tag(1)
                Text("Types").tag(2)
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding()
            
            Group {
                if segments.count == 0 {
                    PlaceholderView(headline: "No Segments Yet!", subHeadline: #"Tap the "+" button in the upper right corner"#)
                        .padding(.top, 100)
                    Spacer()
                }
                else if selectedIndex == 0 {
                    SectionedListView(vm: ByDayViewModel(), cellFactory: CellFactory())
                }
                else if selectedIndex == 1 {
                    SegmentMonthsView(masterVM: MonthsMasterViewModel())
                }
                else if selectedIndex == 2 {
                    SectionedListView(vm: ByTypeViewModel(), cellFactory: CellFactory())
                }
            }
        }
        .sheet(item: $sheetIndex) {
            if $0 == 0 {
                SegmentDetailsView(vm: CreateSegmentViewModel())
            }
            else if $0 == 1 {
                ShareSheet(activityItems: [self.exporter.allToCSV()], applicationActivities: nil).onDisappear { self.sheetIndex = nil }
            }
        }
    }
}
