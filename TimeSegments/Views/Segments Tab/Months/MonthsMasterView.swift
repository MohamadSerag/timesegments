import SwiftUI

struct MonthsMasterView<ViewModel: MonthsMasterViewModeling>: View {
    @ObservedObject var vm: ViewModel
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(spacing: 15) {
                ForEach(vm.months) { month in
                    MonthCell(month: month, selectedIndex: self.$vm.selectedMonthIndex)
                        .padding(.top, 3)
                }
            }
            .padding(.horizontal)
        }
    }
}

struct MonthCell: View {
    let month: MonthInfo
    @Binding var selectedIndex: Int
    
    private var isSelected: Bool { selectedIndex == month.index }
    
    var body: some View {
        Button(action: tapped) {
            VStack {
                ZStack(alignment: .center) {
                    Circle()
                        .stroke(Color.blue, lineWidth: 2)
                    .frame(width: 50, height: 50)
                    
                    if isSelected {
                        Circle()
                            .foregroundColor(.blue)
                            .frame(width: 50, height: 50)
                    }
                    Text(month.name)
                        .font(.headline)
                        .foregroundColor(isSelected ? .white : .primary)
                }
                
                Text(month.year)
                    .foregroundColor(.secondary)
                    .font(.subheadline)
            }
        }
    }
    
    private func tapped() {
        if selectedIndex != month.index {
            selectedIndex = month.index
        }
    }
}

