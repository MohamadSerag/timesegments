import SwiftUI

struct SegmentMonthsView<MasterViewModel: MonthsMasterViewModeling>: View {
    @ObservedObject var masterVM: MasterViewModel
    var selectedMonthIndex: Int {
        let mCount = masterVM.months.count
        if mCount <= masterVM.selectedMonthIndex {
            return mCount - 1
        }
        return masterVM.selectedMonthIndex
    }
    
    var body: some View {
        VStack {
            MonthsMasterView(vm: masterVM)
                .frame(height: 85)
            
            SectionedListView(vm: MonthsDetailsViewModel(monthDate: masterVM.months[selectedMonthIndex].components), cellFactory: CellFactory())
        }
    }
}

struct SegmentMonthlyView_Previews: PreviewProvider {
    static var previews: some View {
        SegmentMonthsView(masterVM: MonthsMasterViewModel())
    }
}
