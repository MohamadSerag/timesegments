import SwiftUI

struct ContextMenuSegmentCell: View {
    var segment: SegmentEntity
    @State private var editSheetIsPresented = false
    
    var body: some View {
        SegmentCell(vm: SegmentCellViewModel(segment: segment))
            .contextMenu {
                ContextMenuEditButton(editViewIsPresented: $editSheetIsPresented)
        }
        .sheet(isPresented: $editSheetIsPresented) { SegmentDetailsView(vm: EditSegmentViewModel(segmentEntity: self.segment)) }
    }
}

struct SegmentCell<ViewModel: SegmentCellViewModeling>: View {
    @ObservedObject var vm: ViewModel
    private let defaults = UserDefaults.standard
    private var bgColor: Color { defaults.colorMode == .capsule ? Color.platterBackground : vm.typeColor.opacity(defaults.opacity) }
    private var titleColor: Color { (defaults.colorMode == .full && vm.whiteLabel) ? .white : .primary }
    
    var body: some View {
        VStack {
            HStack {
                Text(vm.title)
                    .font(.headline)
                    .foregroundColor(titleColor)
                Spacer()
                Text(vm.typeName)
                    .font(.footnote)
                    .foregroundColor(.secondary)
                    .padding(.horizontal, 5)
                    .overlay(Capsule().stroke(Color.secondary, lineWidth: 1))
            }
            Spacer()
            
            //MARK:- Duration
            ZStack {
                if defaults.colorMode == .capsule {
                    Capsule()
                        .foregroundColor(vm.typeColor)
                        .frame(height: 20)
                }
                Text(vm.duration)
                    .foregroundColor(vm.whiteLabel ? .white : .black)
                    .font(.headline)
                    .fontWeight(.bold)
                    .foregroundColor(.durationLabel)
            }
            
            Spacer()
            
            //MARK:- Start/End times
            HStack {
                HStack {
                    Image(systemName: "clock")
                    Text(vm.startTime)
                }
                .foregroundColor(.secondary)
                
                Spacer()
                
                HStack {
                    Image(systemName: "clock")
                        .rotationEffect(.init(degrees: 90))
                    Text(vm.endTime)
                }
                .foregroundColor(.secondary)
            }
            .font(.subheadline)
        }
        .padding(5)
        .background(bgColor)
        .cornerRadius(10)
    }
}

