import SwiftUI

struct TimerView<ViewModel: TimerViewModeling>: View {
    @ObservedObject var vm: ViewModel
    @State private var sheetIndex: Int?
    
    var body: some View {
        VStack {
            Spacer()
            
            //MARK:- Counter
            VStack(spacing: 0) {
                TitleField(text: $vm.title, sheetIndex: $sheetIndex)
                    .offset(x: 15)
                DurationLabel(majorDuration: vm.activeDurationMajor, minorDuration: vm.activeDurationMinor)
                StartTimeLabel(time: vm.startTime, ledColor: vm.ledColor)
            }
            
            Spacer()
            
            //MARK:- Button
            HStack {
                TimerButton(symbolName: vm.endButtonSymbolName, action: endButtonAction)
                    .disabled(!vm.isActive)
                Spacer()
                
                ResetButton(action: vm.reset)
                    .disabled(vm.isActive)
                
                Spacer()
                
                TimerButton(symbolName: vm.startButtonSymbolName, action: vm.startButtonTapped, color: vm.startButtonColor)
            }
        }
        .sheet(item: $sheetIndex) { self.sheet(at: $0) }
    }
    
    private func sheet(at index: Int) -> some View {
        Group {
            if index == 0 {
                SegmentTitlesView(vm: self.vm)
            }
            else {
                SegmentDetailsView(vm: CreateSegmentViewModel(inputSegmentInfo: self.vm.outputSegmentInfo))
            }
        }
    }
    
    private func endButtonAction() {
        vm.endButtonTapped()
        sheetIndex = 1
    }
}

struct TitleField: View {
    @Binding var text: String
    @Binding var sheetIndex: Int?
    
    var body: some View {
        HStack(spacing: 0) {
            TextField("Enter Segment Title", text: $text)
                .autocapitalization(.words)
                .multilineTextAlignment(.center)
                .foregroundColor(.secondary)
            Button(action: {
                self.sheetIndex = 0
            }) {
                Image(systemName: "text.insert")
            }
            .padding(.trailing)
        }
    }
}

struct StartTimeLabel: View {
    let time: String
    var ledColor: Color
    
    var body: some View {
        HStack {
            Image(systemName: "circle.fill")
                .font(.caption)
                .foregroundColor(ledColor)
            
            Text(time)
                .foregroundColor(.secondary)
        }
    }
}

struct DurationLabel: View {
    let majorDuration: String
    let minorDuration: String
    
    var body: some View {
        HStack(alignment: .firstTextBaseline, spacing: 0) {
            Text(majorDuration)
                .font(Font.system(size: 90).monospacedDigit())
            Text(minorDuration)
                .font(Font.body.monospacedDigit())
                .foregroundColor(.secondary)
        }
        .offset(x: 10)
    }
}

struct TimerButton: View {
    let symbolName: String
    let action: () -> ()
    var color = Color.white
    
    var body: some View {
        Button(action: {
            self.action()
        }) {
            ZStack {
                Capsule()
                    .foregroundColor(.blue)
                Image(systemName: symbolName)
                    .foregroundColor(color)
                    .imageScale(.large)
            }
            .frame(width: 80, height: 40)
        }
    }
}

struct ResetButton: View {
    let action: () -> ()
    
    var body: some View {
        Button(action: {
            self.action()
        }) {
            Image(systemName: "gobackward")
                .imageScale(.large)
                .foregroundColor(.orange)
        }
    }
}

struct TimerView_Previews: PreviewProvider {
    static var previews: some View {
        TimerView(vm: TimerViewModel())
    }
}

//MARK:- Helper Extensions
extension Int: Identifiable {
    public var id: Int { self }
}
