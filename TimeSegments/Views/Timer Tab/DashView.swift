import SwiftUI

struct DashView: View {
    @FetchRequest(fetchRequest: SegmentEntity.todaySegmentsFetchReq) var segments: FetchedResults<SegmentEntity>
    
    var body: some View {
        ZStack {
            Color.sceneBackground
                .edgesIgnoringSafeArea(.all)
            
            GeometryReader { gr in
                VStack(alignment: .center) {
                    Spacer()
                    //MARK:- Timer
                    TimerView(vm: TimerViewModel.default)
                        .padding()
                        .frame(height: gr.size.height / 2.2)
                        .background(Color.platterBackground)
                        .cornerRadius(CGFloat.platterCornerRadius)
                        .onTapGesture { UIApplication.shared.endEditing() }
                        .padding(.horizontal)
                    Spacer()
                    
                    //MARK:- Header
                    HStack {
                        Text("Today")
                            .foregroundColor(.secondary)
                        Spacer()
                    }
                    .padding([.leading])
                    Spacer()
                    
                    //MARK:- Today list
                    Group {
                        if self.segments.count == 0 {
                            PlaceholderView(headline: "Nothing Today!", subHeadline: "Start recording right away.")
                                .frame(maxWidth: gr.size.width - 30, maxHeight: .infinity)
                                .background(Color.platterBackground)
                                .cornerRadius(CGFloat.platterCornerRadius)
                            Spacer()
                        } else {
                            SimpleListView(vm: TodayViewModel(), cellFactory: CellFactory())
                        }
                    }
                }
            }
        }
    }
}
