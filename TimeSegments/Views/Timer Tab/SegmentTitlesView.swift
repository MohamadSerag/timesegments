import SwiftUI

struct SegmentTitlesView<ViewModel: SegmentTitleProviding>: View {
    @ObservedObject var vm: ViewModel
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Button("Done") { self.presentationMode.wrappedValue.dismiss() }
            }
            .padding([.leading, .top, .trailing])
            
            List {
                Section(header: Text("Previous Segment Titles")) {
                    Picker("", selection: self.$vm.selectedTitleIndex) {
                        ForEach(0..<self.vm.previousTitles.count, id: \.self) {
                            Text(self.vm.previousTitles[$0]).tag($0)
                        }
                    }
                    .labelsHidden()
                    .pickerStyle(WheelPickerStyle())
                }
                .padding(.top)
            }
            .listStyle(GroupedListStyle())
        }
    }
}

struct SegmentTitlesView_Previews: PreviewProvider {
    static var previews: some View {
        SegmentTitlesView(vm: TimerViewModel())
    }
}
