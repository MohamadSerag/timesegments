import SwiftUI

struct NoteSettingsView<ViewModel: NoteSettingsViewModeling>: View {
    @ObservedObject var vm: ViewModel
    
    var body: some View {
        Section(header: Text("Notifications"), footer: Text(explanation)) {
            Toggle(isOn: $vm.noteIsEnabled) {
                HStack {
                    Image(systemName: "bell.fill")
                        .foregroundColor(.red)
                    Text("Enable Notifications")
                }
            }
            Stepper(value: $vm.repeatInterval, in: 0.25...10, step: 0.25) {
                HStack {
                    Image(systemName: "arrow.clockwise.circle.fill")
                        .foregroundColor(.red)
                    Text("Repeat Interval")
                    Spacer()
                    Text("\(vm.repeatInterval.description) h")
                        .foregroundColor(.secondary)
                }
            }
            .disabled(!vm.noteIsEnabled)
        }
    }
}

 fileprivate let explanation = "When you're recording a segment, the app will send you notifications so you don't lock your phone and forget that there's a segment running in the background."
