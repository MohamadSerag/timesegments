import SwiftUI

struct TypeCell: View {
    @ObservedObject var type: SegmentTypeEntity
    @ObservedObject var colorVM: ColorViewModel
    @State private var editSheetIsPresented = false
    
    var body: some View {
        HStack(spacing: 10) {
            RoundedRectangle(cornerRadius: 10)
                .fill(colorVM.rectColor)
                .frame(width: 45, height: 40)
            
            
            VStack(alignment: .leading) {
                Text(type.name!)
                    .font(.headline)
                Text("\(type.segments.count.description) Segment(s)")
                    .font(.subheadline)
                    .foregroundColor(.secondary)
            }
        }
        .contextMenu {
            ContextMenuEditButton(editViewIsPresented: $editSheetIsPresented)
        }
        .sheet(isPresented: $editSheetIsPresented) { TypeDetailsView(vm: EditTypeViewModel(typeEntity: self.type)) }
    }
    
    init(type: SegmentTypeEntity) {
        self.type = type
        colorVM = ColorViewModel(type: type)
    }
}

class ColorViewModel: ObservableObject {
    @Published var rectColor: Color
    
    init(type: SegmentTypeEntity) {
        if let data = type.colorData {
            let c = Color(try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as! UIColor)
            if UserDefaults.standard.colorMode == .full {
                rectColor = c.opacity(UserDefaults.standard.opacity)
            } else {
                rectColor = c
            }
        } else {
            rectColor = .black
        }
    }
}
