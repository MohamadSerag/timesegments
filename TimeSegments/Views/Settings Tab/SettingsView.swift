import SwiftUI

struct SettingsView: View {
    var body: some View {
        NavigationView {
            List {
                Section {
                    NavigationLink(destination: TypeSettingsView()) {
                        HStack {
                            Image(systemName: "tray.2.fill")
                                .foregroundColor(.orange)
                            Text("Segment Types")
                        }
                    }
                }
                
                NoteSettingsView(vm: NoteSettingsViewModel())
                
                Section {
                    HStack {
                        Text("Version")
                        Spacer()
                        Text("\(Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String)")
                    }
                    .foregroundColor(.secondary)
                }
            }
            .listStyle(GroupedListStyle())
            .navigationBarTitle("Settings")
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
