import SwiftUI

struct TypeDetailsView<ViewModel: TypeDetailsProviding & Saving>: View {
    @ObservedObject var vm: ViewModel
    @Environment(\.presentationMode) private var presentationMode
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Button("Save") {
                    self.vm.save()
                    self.presentationMode.wrappedValue.dismiss()
                }
                .padding([.top, .trailing])
                .disabled(!vm.isValidInput)
            }
            
            ColorPicker(color: $vm.color, strokeWidth: 50)
                .frame(height: 250)
            
            TextField("Enter Type Name", text: $vm.name)
                .multilineTextAlignment(.center)
                .padding(.top, 5)
            
            Form {
                Section {
                    Toggle(isOn: $vm.whiteLabel, label: { Text("Use white label") })
                }
                Section(header: Text("Preview")) {
                    SegmentCell(vm: PreviewCellViewModel(name: vm.name, color: Color(vm.color), whiteLabel: vm.whiteLabel))
                }                
            }
        }
    }
}
