import SwiftUI

struct TypeSettingsView: View {
    @FetchRequest(fetchRequest: SegmentTypeEntity.sortedFetchReq) var types: FetchedResults<SegmentTypeEntity>
    @ObservedObject var vm = GlobalColorViewModel()
    @State private var addSheetIsPresented = false
    
    var body: some View {
        Group {
            if self.types.count > 0 {
                SimpleListView(vm: TypeListViewModel(), cellFactory: CellFactory(), clearBackground: false)
                    .frame(height: 380)
                
                Form {
                    Section(header: Text("Segment Cell Coloring")) {
                        Toggle(isOn: $vm.fillCell) { Text("Fill whole cell") }
                        HStack {
                            Text("Opacity")
                            Slider(value: self.$vm.opacity, in: 0...1)
                                .disabled(!vm.fillCell)
                        }
                    }
                }
            } else {
                PlaceholderView(headline: "Must Add Types!", subHeadline: "Add some types to group segments.")
            }
        }
        .navigationBarTitle("Types", displayMode: .inline)
        .navigationBarItems(trailing: Button(action: { self.addSheetIsPresented.toggle() }, label: { Image(systemName: "plus.circle.fill").imageScale(.large) }))
        .sheet(isPresented: $addSheetIsPresented) { TypeDetailsView(vm: CreateTypeViewModel()) }
    }
}

class GlobalColorViewModel: ObservableObject {
    @Published var fillCell = false { didSet {
        if fillCell {
            UserDefaults.standard.colorMode = .full
        } else {
            UserDefaults.standard.colorMode = .capsule
        }
        }}
    @Published var opacity: Double = 0 { didSet { UserDefaults.standard.opacity = opacity }}
    
    init() {
        fillCell = UserDefaults.standard.colorMode == .capsule ? false : true
        opacity = UserDefaults.standard.opacity == 0 ? 1 : UserDefaults.standard.opacity
    }
    
}
