import SwiftUI

struct DeletionConfirmSheet: ViewModifier {
    @Binding var isPresented: Bool
    var title = "Warning"
    var message = ""
    var deleter: () -> ()

    func body(content: Content) -> some View {
        content
            .actionSheet(isPresented: $isPresented) {
                ActionSheet(title: Text(title), message: Text(message), buttons: [.cancel(), .destructive(Text("Delete"), action: deleter)])
        }
    }
}
