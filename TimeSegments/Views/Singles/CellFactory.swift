import SwiftUI

protocol CellFactoring {
    associatedtype SomeView: View

    func cell(for item: Any) -> SomeView
}

struct CellFactory: CellFactoring {
    func cell(for item: Any) -> some View {
        Group {
            if (item as? SegmentEntity) != nil {
                ContextMenuSegmentCell(segment: item as! SegmentEntity)
            }
            else if (item as? SegmentTypeEntity) != nil {
                TypeCell(type: item as! SegmentTypeEntity)
            }
            else {
                Text("No Cell")
            }
        }
    }
}
