import SwiftUI

extension Color {
    static let sceneBackground = Color("scene-bg")
    static let uiSceneBackground = UIColor(named: "scene-bg")
    static let platterBackground = Color("platter-bg")
    static let durationLabel = Color("duration")
}

extension UIColor {
    var data: Data {
        try! NSKeyedArchiver.archivedData(withRootObject: self, requiringSecureCoding: true)
    }
}

extension Data {
    var color: UIColor? {
        try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(self) as? UIColor
    }
}

extension CGFloat {
    static let platterCornerRadius: CGFloat = 20
    static let platterWidthMargin: CGFloat = 30
}

struct ContextMenuEditButton: View {
    @Binding var editViewIsPresented: Bool
    
    var body: some View {
        Button(action: { self.editViewIsPresented.toggle() }) {
            HStack {
                Text("Edit").imageScale(.small)
                Image(systemName: "pencil.circle")
            }
        }
    }
}

struct PlaceholderView: View {
    let headline: String
    let subHeadline: String
    var useTitle = true
    
    var body: some View {
        VStack {
            Text(headline)
                .font(useTitle ? .title : .headline)
                .foregroundColor(.secondary)
                .bold()
            
            Text(subHeadline)
                .font(.subheadline)
                .foregroundColor(.secondary)
        }
    }
}
