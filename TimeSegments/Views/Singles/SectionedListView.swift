import SwiftUI

typealias SectionedListViewModeling = ListSectionProviding & ListSectionDeleting

struct SectionedListView<ViewModel: SectionedListViewModeling, CFactory: CellFactoring>: View where ViewModel.SectionInfo.Item: Identifiable {
    var vm: ViewModel
    @State private var confirmIsPresented = false
    let cellFactory: CFactory
    
    var body: some View {
        List {
            ForEach(vm.sections, id: \.headerTitle) { section in
                
                Section(header: Text(section.headerTitle)) {
                    // attaching action sheet to each cell to work on iPads.
                    // custom modifier causes weird bug here. Hence, the action sheet
                    ForEach(section.items) { item in
                        self.cellFactory.cell(for: item)
                            .listRowBackground(Color.sceneBackground)
                            .actionSheet(isPresented: self.$confirmIsPresented) {
                                ActionSheet(title: Text("Warning"), message: Text(self.vm.deletionWarning), buttons: [.cancel(), .destructive(Text("Delete"), action: self.vm.delete)])
                        }
                    }
                    .onDelete {
                        self.vm.deletionIndex = $0.first!
                        self.vm.deletionSection = section
                        self.confirmIsPresented.toggle()
                    }
                }
            }
        }
        .listStyle(GroupedListStyle())
    }
}
