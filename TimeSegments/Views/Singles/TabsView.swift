import SwiftUI

struct TabsView: View {
    
    var body: some View {
        TabView {
            
            DashView()
                .tabItem {
                    VStack {
                        Image(systemName: "stopwatch.fill")
                            .font(.system(size: tabFontSize))
                        Text("Timer")
                            .font(.system(size: tabFontSize))
                    }.tag(0)
            }
            
            SegmentTabView()
                .tabItem {
                    Image(systemName: "archivebox.fill")
                        .font(.system(size: tabFontSize))
                    Text("Segments")
                        .font(.system(size: tabFontSize))
            }.tag(1)
            
            SegmentStatsView(vm: SegmentStatsViewModel())
                .tabItem {
                    Image(systemName: "1.magnifyingglass")
                        .font(.system(size: tabFontSize))
                    Text("Zoom")
                        .font(.system(size: tabFontSize))
            }.tag(2)
            
            TotalStatsView(vm: TotalStatsViewModel())
                .tabItem {
                    Image(systemName: "eye.fill")
                        .font(.system(size: tabFontSize))
                    Text("Bird's Eye")
                        .font(.system(size: tabFontSize))
            }.tag(3)
            
            
            SettingsView()
                .tabItem {
                    Image(systemName: "gear")
                        .font(.system(size: tabFontSize))
                    Text("Settings")
                        .font(.system(size: tabFontSize))
            }.tag(4)
            
        }
        .onAppear { NotificationManager.shared.requestAuth(successCompletion: nil) }
    }
}

fileprivate let tabFontSize: CGFloat = 25

struct TabsView_Previews: PreviewProvider {
    static var previews: some View {
        TabsView()
    }
}
