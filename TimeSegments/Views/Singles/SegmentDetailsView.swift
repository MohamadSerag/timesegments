import SwiftUI

struct SegmentDetailsView<ViewModel: SegmentDetailsProviding & Saving>: View {
    @ObservedObject var vm: ViewModel
    @State var titlesIsPresented = false
    @Environment(\.presentationMode) private var presentationMode
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    HStack {
                        TextField("Segment Title", text: $vm.title)
                        Spacer()
                        Button(action: {
                            self.titlesIsPresented.toggle()
                        }) {
                            Image(systemName: "text.insert")
                        }
                    }
                    if titlesIsPresented {
                        Picker(selection: $vm.selectedTitleIndex, label: Text("").font(.subheadline)) {
                            ForEach(0..<vm.previousTitles.count, id: \.self) {
                                Text(self.vm.previousTitles[$0])
                                    .font(.subheadline)
                            }
                        }
                        .pickerStyle(WheelPickerStyle())
                    }
                }
                Section {
                    Picker("Type", selection: $vm.selectedTypeIndex) {
                        ForEach(0..<vm.segmentTypeNames.count, id: \.self) { index in
                            Text(self.vm.segmentTypeNames[index])
                        }
                        
                    }
                    .pickerStyle(DefaultPickerStyle())
                }
                Section(header: Text("Date")) {
                    DatePicker(selection: $vm.startDate, in: ...Date()) { Text("Started On") }
                    DatePicker(selection: $vm.endDate, in: ...Date()) { Text("Ended On") }
                }
                Section(header: Text("Duration")) {
                    Stepper(value: $vm.hoursDuration, in: 0...24) {
                        DurationCellLabel(value: vm.hoursDuration.description, unit: "Hour(s)")
                    }
                    Stepper(value: $vm.minutesDuration, in: 0...59) {
                        DurationCellLabel(value: vm.minutesDuration.description, unit: "Minute(s)")
                    }
                }
            }
            .navigationBarTitle("Segment Details", displayMode: .inline)
            .navigationBarItems(
                leading: Button("Discard") { self.presentationMode.wrappedValue.dismiss() },
                trailing: Button("Save") { self.vm.save(); self.presentationMode.wrappedValue.dismiss() }.disabled(!vm.isValidInput)
            )
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct DurationCellLabel: View {
    let value: String
    let unit: String
    
    var body: some View {
        HStack(alignment: .firstTextBaseline) {
            Text(unit)
                .font(.subheadline)
            Spacer()
            Text(value)
                .foregroundColor(.secondary)
                .font(Font.body.monospacedDigit())
                .padding(.trailing)
        }
    }
}
