import SwiftUI

typealias SimpleListViewModeling = ListItemProviding & ListItemDeleting

struct SimpleListView<ViewModel: SimpleListViewModeling, CFactory: CellFactoring>: View where ViewModel.Entity: Identifiable {
    var vm: ViewModel
    var cellFactory: CFactory = CellFactory() as! CFactory
    var clearBackground = true
    @State private var confirmIsPresented = false
    
    var body: some View {
        List {
            ForEach(vm.items) { seg in
                if self.clearBackground {
                    self.cellFactory.cell(for: seg)
                        .listRowBackground(Color.sceneBackground)
                }
                else {
                    self.cellFactory.cell(for: seg)
                }
            }
            .onDelete(perform: self.deleteTapped(at:))
            
            // custom modifier causes weird bug here. Hence, the action sheet
            .actionSheet(isPresented: self.$confirmIsPresented) {
                ActionSheet(title: Text("Warning"), message: Text(self.vm.deletionWarning), buttons: [.cancel(), .destructive(Text("Delete"), action: self.vm.delete)])
            }
        }
    }
    
    private func deleteTapped(at set: IndexSet) {
        self.vm.setForDeletionIndex = set.first!
        self.confirmIsPresented.toggle()
    }
}
