import CoreData

class TypeListViewModel: ListItemProviding, ListItemDeleting, MOCUser {
    var setForDeletionIndex: Int?
    var deletionWarning: String { "Deleting this type will delete ALL related segments, are you sure?" }
    var items: [SegmentTypeEntity] {
        try! moc.fetch(SegmentTypeEntity.sortedFetchReq)
    }
}

