import Foundation

protocol TypeInfoProviding {
    var name: String? { get }
    var colorData: Data? { get }
    var whiteLabel: Bool { get }
}

struct UserTypeInfo: TypeInfoProviding {
    static var `default`: UserTypeInfo { UserTypeInfo(name: nil, colorData: nil, whiteLabel: false) }
    
    let name: String?
    let colorData: Data?
    let whiteLabel: Bool
}
