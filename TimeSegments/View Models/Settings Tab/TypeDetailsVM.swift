import CoreData
import SwiftUI

//MARK:- Interface
protocol TypeDetailsProviding: ObservableObject {
    var name: String { get set }
    var color: UIColor { get set }
    var whiteLabel: Bool { get set }
    var fillCell: Bool { get set }
    var isValidInput: Bool { get }
}

//MARK:- Internal works
protocol TypeDetailsViewModel: TypeDetailsProviding {
    var outputTypeInfo: UserTypeInfo { get }
    
    func populate(with info: TypeInfoProviding)
    func updateUserDefaults()
}

//MARK:- Default Implementation
extension TypeDetailsProviding {
    var isValidInput: Bool { !name.isEmpty }
}

extension TypeDetailsViewModel where Self: MOCUser {
    var outputTypeInfo: UserTypeInfo {
        UserTypeInfo(name: name, colorData: color.data, whiteLabel: whiteLabel)
    }
    
    func populate(with info: TypeInfoProviding) {
        name = info.name ?? ""
        color = info.colorData?.color ?? .red
        whiteLabel = info.whiteLabel
        if UserDefaults.standard.colorMode == .full {
            fillCell = true
        }
    }
    
    func updateUserDefaults() {
        if fillCell {
            UserDefaults.standard.colorMode = .full
        } else {
            UserDefaults.standard.colorMode = .capsule
        }
    }
}

//MARK:- Helper Extensions
extension NSManagedObjectContext {
    func createType(with info: TypeInfoProviding) {
        update(SegmentTypeEntity(context: self), with: info)
    }
    
    func update(_ type: SegmentTypeEntity, with info: TypeInfoProviding) {
        type.name = info.name
        type.colorData = info.colorData
        type.whiteLabel = info.whiteLabel
        try! save() // to avoid the crash on deleting newly added segment
    }
}

