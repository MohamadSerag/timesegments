import CoreData
import SwiftUI

class CreateTypeViewModel: TypeDetailsProviding, Saving, MOCUser, TypeDetailsViewModel  {
    @Published var name = ""
    @Published var color: UIColor = .orange
    @Published var whiteLabel: Bool = false
    @Published var fillCell: Bool = false { didSet { updateUserDefaults() }}
        
    init(inputTypeInfo: TypeInfoProviding = UserTypeInfo.default) {
        populate(with: inputTypeInfo)
    }
    
    func save() {
        moc.createType(with: outputTypeInfo)
    }
}

class EditTypeViewModel: TypeDetailsProviding, Saving, MOCUser, TypeDetailsViewModel  {
    @Published var name = ""
    @Published var color = UIColor.clear
    @Published var whiteLabel: Bool = false
    @Published var fillCell: Bool = false { didSet { updateUserDefaults() }}
    
    private let typeEntity: SegmentTypeEntity
    
    init(typeEntity: SegmentTypeEntity) {
        self.typeEntity = typeEntity
        populate(with: typeEntity)
    }
    
    func save() {
        moc.update(typeEntity, with: outputTypeInfo)
    }
}

