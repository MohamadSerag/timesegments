import Combine
import Foundation

protocol NoteSettingsViewModeling: ObservableObject {
    var noteIsEnabled: Bool { get set }
    var repeatInterval: Double { get set }
}

class NoteSettingsViewModel: NoteSettingsViewModeling {
    @Published var noteIsEnabled: Bool { didSet { userDefaults.noteIsEnabled = noteIsEnabled } }
    @Published var repeatInterval: Double { didSet { userDefaults.noteRepeatInterval = repeatInterval} }
    
    private let userDefaults = UserDefaults.standard
    
    init() {
        noteIsEnabled = userDefaults.noteIsEnabled
        let ri = userDefaults.noteRepeatInterval
        repeatInterval = ri == 0 ? 0.25 : ri
    }
}

//MARK:- Helper Extensions
extension UserDefaults: ObservableObject {
    var noteIsEnabled: Bool {
        get { bool(forKey: "NoteIsEnabledKey") }
        set { set(newValue, forKey: "NoteIsEnabledKey")}
    }
    var noteRepeatInterval: Double {
        get { double(forKey: "RepeatIntervalKey") }
        set { set(newValue, forKey: "RepeatIntervalKey")}
    }
    var colorMode: CellColorMode {
        get { bool(forKey: "FillCellKey") ? .full : .capsule }
        set { set(newValue == .capsule ? false : true, forKey: "FillCellKey"); objectWillChange.send() }
    }
    var opacity: Double {
        get { double(forKey: "OpacityKey") }
        set { set(newValue, forKey: "OpacityKey"); objectWillChange.send() }
    }
}

extension UserDefaults {
    enum CellColorMode {
        case capsule, full
    }
}
