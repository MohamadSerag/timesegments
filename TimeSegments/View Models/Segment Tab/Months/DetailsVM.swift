import CoreData

class MonthsDetailsViewModel: ByDayViewModel {
    override var sortedSegments: [SegmentEntity] { moc.segments(in: monthDate) }
    private let monthDate: DateComponents
    
    init(monthDate: DateComponents) {
        self.monthDate = monthDate
        super.init()
    }
}

//MARK:- Helper Extensions
extension NSManagedObjectContext {
    func segments(in month: DateComponents) -> [SegmentEntity] {
        let bounds = Calendar.current.monthBounds(for: month)
        let req: NSFetchRequest<SegmentEntity> = SegmentEntity.fetchRequest()
        req.sortDescriptors = [NSSortDescriptor(keyPath: \SegmentEntity.startDate!, ascending: false)]
        req.predicate = NSPredicate(format: "startDate >= %@ && startDate <= %@", argumentArray: [bounds.start, bounds.end])
        return try! fetch(req)
    }
}

extension Calendar {
    func monthBounds(for dateComps: DateComponents) -> (start: Date, end: Date) {
        let start = date(from:  dateComponents([.year, .month], from: date(from: dateComps)!))
        var modifier = DateComponents()
        modifier.month = 1
        let end = date(byAdding: modifier, to: start!)
        return (start!, end!)
    }
}


