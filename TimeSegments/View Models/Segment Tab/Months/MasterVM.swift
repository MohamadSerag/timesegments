import CoreData

//MARK:- Interface
protocol MonthsMasterViewModeling: ObservableObject {
    var months: [MonthInfo] { get }
    var selectedMonthIndex: Int { get set }
}

//MARK:- Concrete Implementation
class MonthsMasterViewModel: MonthsMasterViewModeling, MOCUser {
    static var lastSelectedMonthIndex = 0
    
    @Published var selectedMonthIndex: Int { didSet { MonthsMasterViewModel.lastSelectedMonthIndex = selectedMonthIndex }}
    var months: [MonthInfo] {
        return Dictionary(grouping: moc.segmentsSortedByStartDate()) { seg -> DateComponents in Calendar.current.dateComponents([.year, .month], from: seg.startDate!) }
            .sorted { Calendar.current.date(from: $0.0)! > Calendar.current.date(from: $1.0)! }
            .map { $0.key }
            .enumerated()
            .map { (index, components) in MonthInfo(components: components, index: index) }
    }
        
    init() {
        selectedMonthIndex = MonthsMasterViewModel.lastSelectedMonthIndex
    }
}

//MARK:- Output Type
struct MonthInfo: Identifiable {
    let components: DateComponents
    var name: String { components.monthDescription }
    var year: String { components.yearDescription }
    let index: Int
    var id: Int { index }
}

//MARK:- Helper Extensions
extension DateComponents {
    var monthDescription: String {
        return String(DateFormatter.monthDateFormatter.string(from: Calendar.current.date(from: self)!).prefix(3))
    }
    var yearDescription: String {
        self.year!.description
    }
}

extension DateFormatter {
    static var monthDateFormatter: DateFormatter = {
        let f = DateFormatter()
        f.dateFormat = "MMMM"
        return f
    }()
}
