import CoreData

class ByDayViewModel: ListSectionProviding, ListSectionDeleting, MOCUser, GroupedSegmentsProvider {
    var sections: [SegmentSection] {
        sortedTuples(grouping: sortedSegments, by: groupKey(from:), ascending: false)
        .map { SegmentSection(headerTitle: $0.key.dayDescription, items: $0.value) }
    }
    var deletionIndex: Int?
    var deletionSection: SegmentSection?
    
    //MARK:- Private
    var calendar = Calendar.current
    var sortedSegments: [SegmentEntity] { moc.segmentsSortedByStartDate() }
}

class ByTypeViewModel: ListSectionProviding, ListSectionDeleting, MOCUser {
    var sections: [SegmentSection] {
        try! moc.fetch(SegmentTypeEntity.sortedFetchReq)
            .filter { $0.segments.count > 0 }
            .map { SegmentSection(headerTitle: $0.name!, items: $0.segments) }
    }
    var deletionIndex: Int?
    var deletionSection: SegmentSection?
}

//MARK:- Helper Extensions
extension DateComponents {
    var dayDescription: String {
        DateFormatter.dayDateFormatter.string(from: Calendar.current.date(from: self)!)
    }
}

extension DateFormatter {
    static var dayDateFormatter: DateFormatter = {
        let f = DateFormatter()
        f.dateStyle = .medium
        return f
    }()
}

