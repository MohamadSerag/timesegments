import CoreData
import Combine

protocol MOCSubscriber: AnyObject {
    associatedtype Entity: NSManagedObject
    
    var subs: Set<AnyCancellable> { get set }
    var noteCenter: NotificationCenter { get }

    func subscribe()
    func receive(_ note: Notification)
    func handle()
}

//MARK:- Default Implementation
extension MOCSubscriber where Self: MOCUser {
    var noteCenter: NotificationCenter { NotificationCenter.default }
    
    func subscribe() {
        noteCenter.publisher(for: .NSManagedObjectContextObjectsDidChange, object: moc)
            .sink {[weak self] in self?.receive($0) }
            .store(in: &subs)
    }
}

extension MOCSubscriber  {
    var keys: [String] { [NSInsertedObjectsKey, NSUpdatedObjectsKey, NSDeletedObjectsKey] }
    
    func receive(_ note: Notification) {
        keys.forEach {
            if let entities = (note.userInfo?[$0] as? Set<NSManagedObject>)?.compactMap({ $0 as? Entity }), entities.count > 0 {
                handle()
                return
            }
        }
    }
}


