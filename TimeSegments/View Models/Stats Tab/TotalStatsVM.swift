import CoreData
import Combine

//MARK:- Interface
protocol TotalStatsViewModeling: ObservableObject {
    var startDate: Date { get set }
    var endDate: Date { get set }
    var dateRange: ClosedRange<Date> { get }
    var includeNoRecords: Bool { get set }
    var periodSegments: [SegmentEntity] { get }
}

class TotalStatsViewModel: TotalStatsViewModeling, MOCUser, MOCSubscriber {
    typealias Entity = SegmentEntity
    
    var dateRange: ClosedRange<Date> {
        let list = moc.segmentsSortedByStartDate()
        return (list.last?.startDate ?? Date()) ... (list.first?.startDate ?? Date())
    }
    @Published var startDate = Date()
    @Published var endDate = Date()
    @Published var includeNoRecords = false
    var periodSegments: [SegmentEntity] {
         moc.fetchSegments(from: calendar.startOfDay(for: startDate), to: calendar.endOfDay(for: endDate))
    }
    
    var subs: Set<AnyCancellable> = []
    private let calendar = Calendar.current
    
    init() {
        subscribe()
        updateDates()
    }
    
    private func updateDates() {
        let range = dateRange
        startDate = range.lowerBound
        endDate = range.upperBound
    }
    
    func handle() {
        updateDates()
    }
}

//MARK:- Helper Extensions
extension NSManagedObjectContext {
    func fetchSegments(from startDate: Date, to endDate: Date) -> [SegmentEntity] {
        let req: NSFetchRequest<SegmentEntity> = SegmentEntity.fetchRequest()
        req.predicate = NSPredicate(format: "startDate >= %@ && startDate <= %@", argumentArray: [startDate, endDate])
        req.sortDescriptors = [NSSortDescriptor(keyPath: \SegmentEntity.startDate!, ascending: false)]
        return try! fetch(req)
    }
}

extension Calendar {
    func endOfDay(for date: Date) -> Date {
        let nextMidnight = startOfDay(for: self.date(byAdding: .day, value: 1, to: date)!)
        return self.date(byAdding: .second, value: -1, to: nextMidnight)!
    }
}
