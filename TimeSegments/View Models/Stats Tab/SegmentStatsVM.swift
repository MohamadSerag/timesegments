import CoreData

//MARK:- Interface
protocol SegmentStatsViewModeling: SegmentTitleProviding {
    var barInfoList: [BarCellInfo] { get }
    var totalDuration: String { get }
    var startDate: Date { get set }
    var endDate: Date { get set }
    
    var startDateRange: PartialRangeThrough<Date> { get }
    var endDateRange: ClosedRange<Date> { get }
}

//MARK:- Concrete
class SegmentStatsViewModel: SegmentStatsViewModeling, MOCUser, GroupedSegmentsProvider {
    var barInfoList: [BarCellInfo] {
        let calc = SegmentBarCalc(allSegments: matchingSegments)
        return (0...calendar.periodDaysCount(from: startDate, to: endDate))
            .map {
                let currentDate = calendar.date(byAdding: .day, value: $0, to: startDate)!
                let data = calc.calculations(for: currentDate)
                return BarCellInfo(dayName: calendar.dayName(for: currentDate), lengthPercent: data.percent, hours: data.hours, minutes: data.minutes)
        }
    }
    var totalDuration: String {
        let sum = Int(matchingSegments.sumInMinutes())
        let hours = sum / 60
        let minutes = sum % 60
        
        var minuteStr = "00"
        if minutes > 0 && minutes < 10 {
            minuteStr = "0\(minutes.description)"
        }
        else if minutes > 10 {
            minuteStr = minutes.description
        }
        return "\(hours.description)h : \(minuteStr)m"
        
    }
    @Published var startDate: Date = Calendar.current.date(byAdding: .day, value: -7, to: Date())!
    @Published var endDate: Date = Date()
    @Published var selectedTitleIndex: Int = 0
    var startDateRange: PartialRangeThrough<Date> { ...Calendar.current.date(byAdding: .day, value: -1, to: endDate)! }
    var endDateRange: ClosedRange<Date> { Calendar.current.date(byAdding: .day, value: 1, to: startDate)!...Date() }
    
    private let calendar = Calendar.current
    private var matchingSegments: [SegmentEntity] {
        let req: NSFetchRequest<SegmentEntity> = SegmentEntity.fetchRequest()
        let argList: [Any] = [previousTitles[selectedTitleIndex], calendar.startOfDay(for: startDate), calendar.endOfDay(for: endDate)]
        req.predicate = NSPredicate(format: "title == %@ && startDate >= %@ && startDate <= %@", argumentArray: argList)
        req.sortDescriptors = [NSSortDescriptor(keyPath: \SegmentEntity.startDate!, ascending: true)]
        return try! moc.fetch(req)
    }
}

//MARK:- Helpers
extension Calendar {
    func dayName(for date: Date) -> String {
        let f = DateFormatter()
        f.dateFormat = "E"
        return f.string(from: date)
    }
    
    func periodDaysCount(from start: Date, to end: Date) -> Int {
        dateComponents(
            [.day, .month, .year],
            from: dateComponents([.day, .month, .year], from: start),
            to: dateComponents([.day, .month, .year], from: end)
        ).day!
    }
}

extension Collection where Self.Element == SegmentEntity {
    func detailedSum() -> (hours: Int, minutes: Int) {
        return (hours: map{$0.activeHours!}.reduce(0, +),
                minutes: map{$0.activeMinutes!}.reduce(0, +))
    }
}
