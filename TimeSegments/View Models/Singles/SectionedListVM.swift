import CoreData
import Combine

//MARK:- Interface
protocol ListSectionProviding {
    associatedtype SectionInfo: SectionInfoProviding
    
    var sections: [SectionInfo] { get }
}

protocol ListSectionDeleting: AnyObject {
    associatedtype SectionInfo: SectionInfoProviding
    
    var deletionIndex: Int? { get set }
    var deletionSection: SectionInfo? { get set }
    var deletionWarning: String { get }
    
    func delete()
}

protocol SectionInfoProviding {
    associatedtype Item: NSManagedObject
    
    var headerTitle: String { get }
    var items: [Item] { get }
}

//MARK:- Default Implementations
extension ListSectionDeleting {
    var deletionWarning: String { "Are you sure you want to delete this segment?" }
}

extension ListSectionDeleting where Self: MOCUser & ListSectionProviding {
    func delete() {
        guard let index = deletionIndex else { return }
        guard let sec = deletionSection else { return }
        moc.firmDelete(sec.items[index])
        deletionIndex = nil
        deletionSection = nil
    }
}

struct SegmentSection: SectionInfoProviding {
    let headerTitle: String
    let items: [SegmentEntity]
}
