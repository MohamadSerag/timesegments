import Foundation

protocol GroupedSegmentsProvider {
    func sortedTuples(grouping segs: [SegmentEntity], by: (SegmentEntity) -> DateComponents, ascending: Bool) -> [(key: DateComponents, value: [SegmentEntity])]
    func groupKey(from seg: SegmentEntity) -> DateComponents
}

extension GroupedSegmentsProvider {
    func sortedTuples(grouping segs: [SegmentEntity], by: (SegmentEntity) -> DateComponents, ascending: Bool = true) -> [(key: DateComponents, value: [SegmentEntity])] {
        if ascending {
            return Dictionary(grouping: segs, by: groupKey(from:))
                .sorted { Calendar.current.date(from: $0.0)! < Calendar.current.date(from: $1.0)! }
        }
        return Dictionary(grouping: segs, by: groupKey(from:))
            .sorted { Calendar.current.date(from: $0.0)! > Calendar.current.date(from: $1.0)! }
    }
    
    func groupKey(from seg: SegmentEntity) -> DateComponents {
        Calendar.current.dateComponents([.year, .month, .day], from: seg.startDate!)
    }
}

