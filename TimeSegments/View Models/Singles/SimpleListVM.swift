import Combine
import CoreData

//MARK:- Interface
protocol ListItemProviding {
    associatedtype Entity: NSManagedObject
    
    var items: [Entity] { get }
}

protocol ListItemDeleting: AnyObject {
    var deletionWarning: String { get }
    var setForDeletionIndex: Int? { get set }
    
    func delete()
}

//MARK:- Default Implementation
extension ListItemDeleting where Self: ListItemProviding & MOCUser {
    func delete() {
        guard let index = setForDeletionIndex else { return }
        moc.delete(items[index])
        setForDeletionIndex = nil
    }
}
