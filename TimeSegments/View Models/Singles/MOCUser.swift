import CoreData

//MARK:- Common Functionality
protocol MOCUser {
    var moc: NSManagedObjectContext { get set }
}

extension MOCUser {
    var moc: NSManagedObjectContext {
        get { CoreDataStoreManager.shared.viewContext }
        set { }
    }
}
