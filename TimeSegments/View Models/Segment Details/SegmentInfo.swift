import Foundation

protocol SegmentInfoProviding {
    var startDate: Date? { get }
    var activeHours: Int? { get }
    var activeMinutes: Int? { get }
    var endDate: Date? { get }
    var title: String? { get }
    var type: SegmentTypeEntity? { get }
}

struct UserSegmentInfo: SegmentInfoProviding {
    static var `default`: UserSegmentInfo { UserSegmentInfo(startDate: nil, activeHours: nil, activeMinutes: nil, endDate: nil, title: nil, type: nil) }
    
    let startDate: Date?
    let activeHours: Int?
    let activeMinutes: Int?
    let endDate: Date?
    let title: String?
    let type: SegmentTypeEntity?
}
