import CoreData
import Combine

//MARK:- Interface
protocol SegmentTitleProviding: ObservableObject {
    var title: String { get set }
    var previousTitles: [String] { get }
    var selectedTitleIndex: Int { get set }
}

//MARK:- Common Functionality
protocol SegmentTitleProvider: SegmentTitleProviding {
    func updateTitle()
}

//MARK:- Default Implementation
extension SegmentTitleProviding where Self: MOCUser {
    var title: String { get {""} set {}}
    var previousTitles: [String] {
        var titles = Array(Set(try! moc.fetch(SegmentEntity.fetchRequest()).compactMap { $0.title })).sorted()
        titles.insert("", at: 0)
        return titles
    }
    
}

extension SegmentTitleProvider where Self: MOCUser {
    var previousTitles: [String] {
        var titles = Array(Set(try! moc.fetch(SegmentEntity.fetchRequest()).compactMap { $0.title })).sorted()
        titles.insert("", at: 0)
        return titles
    }
    
    func updateTitle() {
        title = previousTitles[selectedTitleIndex]
    }
}
