import CoreData
import SwiftUI

protocol SegmentDetailsProviding: SegmentTitleProviding {
    var segmentTypeNames: [String] { get }
    var selectedTypeIndex: Int { get set }
    var startDate: Date { get set }
    var endDate: Date { get set }
    var hoursDuration: Int { get set }
    var minutesDuration: Int { get set }
    var isValidInput: Bool { get }
}

protocol Saving {
    func save()
}

protocol SegmentDetailsViewModel: SegmentDetailsProviding, SegmentTitleProvider {
    var segmentTypes: [SegmentTypeEntity] { get }
    var outputSegmentInfo: UserSegmentInfo { get }
    
    func typeIndex(from info: SegmentInfoProviding) -> Int  // customization point
    func populate(with info: SegmentInfoProviding)
}

//MARK:- Default Implementation
extension SegmentDetailsViewModel where Self: MOCUser {
    var isValidInput: Bool {
        !title.isEmpty &&
            segmentTypes.count > 0 &&
            Calendar.current.compare(startDate, to: endDate, toGranularity: .minute) != .orderedSame &&
            (minutesDuration > 0 || hoursDuration > 0)
    }
    var segmentTypes: [SegmentTypeEntity] { try! moc.fetch(SegmentTypeEntity.sortedFetchReq) }
    var segmentTypeNames: [String] { segmentTypes.map { $0.name ?? "Unknown" } }
    var outputSegmentInfo: UserSegmentInfo {
        UserSegmentInfo(startDate: startDate, activeHours: hoursDuration, activeMinutes: minutesDuration, endDate: endDate, title: title, type: segmentTypes[selectedTypeIndex])
    }
    
    func populate(with info: SegmentInfoProviding) {
        startDate = info.startDate ?? Date()
        hoursDuration = info.activeHours ?? 0
        minutesDuration = info.activeMinutes ?? 0
        endDate = info.endDate ?? Date()
        title = info.title ?? ""
        selectedTypeIndex = typeIndex(from: info)
    }
}

//MARK:- Helper Extensions
extension NSManagedObjectContext {
    func createSegment(with info: UserSegmentInfo) {
        update(SegmentEntity(context: self), with: info)
    }
    
    func update(_ segment: SegmentEntity, with info: UserSegmentInfo) {
        let duration = SegmentDurationEntity(context: self)
        duration.hours = info.activeHours ?? 0
        duration.minutes = info.activeMinutes ?? 0
        
        segment.startDate = info.startDate
        segment.activeDuration = duration
        segment.endDate = info.endDate
        segment.title = info.title
        segment.type = info.type
        try! save() // to avoid the crash on deleting newly added segment
    }
}
