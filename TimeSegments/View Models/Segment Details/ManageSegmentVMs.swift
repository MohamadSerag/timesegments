import CoreData

class CreateSegmentViewModel: SegmentDetailsProviding, Saving, MOCUser, SegmentDetailsViewModel  {
    @Published var title = ""
    @Published var selectedTitleIndex: Int = 0 { didSet { updateTitle() } }
    @Published var selectedTypeIndex = 0
    @Published var startDate = Date()
    @Published var endDate = Date()
    @Published var hoursDuration = 0
    @Published var minutesDuration = 0
    
    init(inputSegmentInfo: SegmentInfoProviding = UserSegmentInfo.default) {
        populate(with: inputSegmentInfo)
    }
    
    func typeIndex(from info: SegmentInfoProviding) -> Int {
        let titlePredicate = NSPredicate(format: "title = %@", argumentArray: [info.title ?? ""])
        let req = SegmentEntity.sortedFetchReq
        req.fetchLimit = 1
        req.predicate = titlePredicate
        guard let typeFromName = try! moc.fetch(req).first?.type else { return 0 }
        return segmentTypes.firstIndex(of: typeFromName)!
    }
    
    func save() {
        moc.createSegment(with: outputSegmentInfo)
    }
}

class EditSegmentViewModel: SegmentDetailsProviding, SegmentDetailsViewModel, Saving, MOCUser {
    @Published var title = ""
    @Published var selectedTitleIndex: Int = 0 { didSet { title = previousTitles[selectedTitleIndex] }}
    @Published var selectedTypeIndex = 0
    @Published var startDate = Date()
    @Published var endDate = Date()
    @Published var hoursDuration = 0
    @Published var minutesDuration = 0
    
    private var segmentEntity: SegmentEntity
    
    init(segmentEntity: SegmentEntity) {
        self.segmentEntity = segmentEntity
        populate(with: segmentEntity)
    }
    
    func typeIndex(from info: SegmentInfoProviding) -> Int {
        segmentTypes.firstIndex(of: info.type!)!
    }
    
    func save() {
        moc.update(segmentEntity, with: outputSegmentInfo)
    }
}



