import CoreData
import Combine

class TodayViewModel: ListItemProviding, ListItemDeleting, MOCUser {
    var setForDeletionIndex: Int?
    var deletionWarning: String { "Are you sure you want to delete this segment?" }
    var items: [SegmentEntity] { try! moc.fetch(SegmentEntity.todaySegmentsFetchReq) }
}
