import Foundation

protocol TimerDurationFormatting {
    func string(from components: DateComponents?) -> (String, String)?
}

struct DurationFormatter: TimerDurationFormatting {
    func string(from components: DateComponents?) -> (String, String)? {
        guard let duration = components else { return nil }
        let hourString = duration.hour ?? 0 > 9 ? duration.hour?.description ?? "00" : "0\(duration.hour?.description ?? "0")"
        let minuteString = duration.minute ?? 0 > 9 ? duration.minute?.description ?? "00" : "0\(duration.minute?.description ?? "0")"
        let secondString = duration.second ?? 0 > 9 ? duration.second?.description ?? "00" : "0\(duration.second?.description ?? "0")"
        return ("\(hourString):\(minuteString)", "\(secondString)")
    }
}

//MARK:-
protocol TimerDateFormatting {
    func timeString(from date: Date?) -> String?
}

extension DateFormatter: TimerDateFormatting {
    static var timeDateFormatter: DateFormatter = {
        let f = DateFormatter()
        f.timeStyle = .short
        return f
    }()
    
    func timeString(from date: Date?) -> String? {
        guard let sureDate = date else { return nil }
        return string(from: sureDate)
    }
}

