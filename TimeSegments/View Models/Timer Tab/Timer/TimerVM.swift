import CoreData
import Combine
import SwiftUI

protocol TimerViewModeling: SegmentTitleProviding {
    var startTime: String { get }
    var activeDurationMajor: String { get }
    var activeDurationMinor: String { get }
    var startButtonSymbolName: String { get }
    var endButtonSymbolName: String { get }
    var isActive: Bool { get }
    var ledColor: Color { get }
    var startButtonColor: Color { get }
    var outputSegmentInfo: UserSegmentInfo { get }
    
    func startButtonTapped()
    func endButtonTapped()
    func reset()
}

//MARK:- Concrete Class
class TimerViewModel: TimerViewModeling, MOCUser, SegmentTitleProvider {
    static let `default` = TimerViewModel()  // to stop "DashView" from creating new vm while segment is running
    
    //MARK:- Interface Properties
    var startTime = "0:00 AM"
    var activeDurationMajor = "00:00"
    @Published var activeDurationMinor = "00"
    @Published var title = ""
    @Published var selectedTitleIndex = 0 { didSet { updateTitle() } }
    var ledColor: Color { state.color }
    var startButtonColor: Color { state == .inactive ? .red : state.next.color }
    var startButtonSymbolName: String { state.symbolName.0 }
    var endButtonSymbolName: String { state.symbolName.1 }
    var isActive: Bool { state != .inactive }
    var outputSegmentInfo: UserSegmentInfo {
        UserSegmentInfo(
            startDate: timer.startDate,
            activeHours: timer.activeDuration?.hour,
            activeMinutes: timer.activeDuration?.minute,
            endDate: Date(),
            title: title,
            type: nil
        )
    }
    
    //MARK:- Global Dependencies
    internal let userDefault = UserDefaults.standard
    
    //MARK:- Private Properties
    @Published private var state: State = .inactive
    private let dateFormatter: TimerDateFormatting
    private let durationFormatter: TimerDurationFormatting
    private let timer: SegmentTimer
    private let noteManager: NotificationManaging
    private var subs = Set<AnyCancellable>()
    
    init(timer: SegmentTimer = SegmentTimer(),
         dateFormatter: TimerDateFormatting = DateFormatter.timeDateFormatter,
         durationFormatter: TimerDurationFormatting = DurationFormatter(),
         notificationManager: NotificationManaging = NotificationManager.shared) {

        self.timer = timer
        self.dateFormatter = dateFormatter
        self.durationFormatter = durationFormatter
        noteManager = notificationManager
        
        timer.$startDate
            .compactMap(dateFormatter.timeString(from:))
            .sink { [weak self] value in self?.startTime = value }
            .store(in: &subs)
        
        timer.$activeDuration
            .compactMap(durationFormatter.string(from:))
            .sink { [weak self] tuple in
                self?.activeDurationMajor = tuple.0
                self?.activeDurationMinor = tuple.1
        }
        .store(in: &subs)
    }
    
    //MARK:- Interface Functions
    func startButtonTapped() {
        timer.actionsInSequence[State.index(of: state)]()
        state = state.next
        if userDefault.noteIsEnabled {
            noteManager.scheduleNotification(withTitle: "Active Segment", body: "Just reminding that \(title.isEmpty ? "a" : title) segment is running.", repeatInterval: userDefault.noteRepeatInterval * 3600)
        }
    }
    
    func endButtonTapped() {
        timer.end()
        noteManager.removeNotifications()
        state = .inactive
    }
    
    func reset() {
        startTime = "0:00 AM"
        activeDurationMajor = "00:00"
        activeDurationMinor = "00"
        title = ""
    }
}

extension TimerViewModel {
    
    enum State: CaseIterable {
        static func index(of state: State) -> Int {
            allCases.firstIndex(of: state)!
        }
        
        case inactive, active, paused
        
        var color: Color {
            switch self {
            case .inactive:
                return Color(#colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1))
            case .active:
                return Color.green
            case .paused:
                return Color.yellow
            }
        }
        var symbolName: (String, String) {
            switch self {
            case .inactive:
                return ("circle.fill", "stop.fill")
            case .active:
                return ("pause.fill", "stop.fill")
            case .paused:
                return ("play.fill", "stop.fill")
            }
        }
        var next: State {
            switch self {
            case .inactive:
                return .active
            case .active:
                return .paused
            case .paused:
                return .active
            }
        }
    }
}
