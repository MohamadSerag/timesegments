import Foundation
import UserNotifications

protocol NotificationManaging {
    func requestAuth(successCompletion: (() -> ())?)
    func scheduleNotification(withTitle title: String, body: String, repeatInterval: Double)
    func removeNotifications()
}

class NotificationManager: NotificationManaging {
    static let shared = NotificationManager()
    
    //MARK:- Global Dependencies
    internal var center = UNUserNotificationCenter.current()
    internal var calendar = Calendar.current
    internal var userDefaults = UserDefaults.standard
    
    private func request(withTitle title: String, body: String, repeatInterval: Double) -> UNNotificationRequest {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = .default
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: repeatInterval, repeats: true)
        return UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
    }
    
    private func add(_ request: UNNotificationRequest) {
        center.removeAllPendingNotificationRequests()
        
        center.getNotificationSettings { [weak self] settings in
            guard (settings.authorizationStatus == .authorized) || (settings.authorizationStatus == .provisional) else { return }
            self?.center.add(request) { _ in }
        }
    }
    
    func requestAuth(successCompletion: (() -> ())? = nil) {
        center.requestAuthorization(options: [.sound, .badge, .alert]) { (isGranted, error) in
            if isGranted {
                self.userDefaults.noteIsEnabled = true
                self.userDefaults.noteRepeatInterval = 0.5
                successCompletion?()
            }
        }
        userDefaults.didRequestAuth = true
    }
    
    func scheduleNotification(withTitle title: String, body: String, repeatInterval: Double) {
        if !userDefaults.didRequestAuth {
            requestAuth { [weak self] in
                guard let sureSelf = self else { return }
                sureSelf.add(sureSelf.request(withTitle: title, body: body, repeatInterval: repeatInterval))
            }
        }
        else {
            self.add(self.request(withTitle: title, body: body, repeatInterval: repeatInterval))
        }
    }
    
    func removeNotifications() {
        center.removeAllDeliveredNotifications()
        center.removeAllPendingNotificationRequests()
    }
}

//MARK:- Helper Extension
extension UserDefaults {
    var didRequestAuth: Bool {
        get { bool(forKey: "AuthRequested") }
        set { set(newValue, forKey: "AuthRequested") }
    }
}
