import SwiftUI

protocol SegmentCellViewModeling: ObservableObject {
    var title: String { get }
    var typeName: String { get }
    var typeColor: Color { get }
    var whiteLabel: Bool { get }
    var duration: String { get }
    var startTime: String { get }
    var endTime: String { get }
}

class SegmentCellViewModel: SegmentCellViewModeling {
    var title: String
    var typeName: String
    var typeColor: Color
    var whiteLabel: Bool
    var duration: String
    var startTime: String
    var endTime: String
    
    internal var timeFormatter: TimerDateFormatting = DateFormatter.timeDateFormatter
    
    init(segment: SegmentEntity) {
        title = segment.title ?? "Unknown"
        typeName = segment.type?.name ?? "Unknown"
        typeColor = Color(segment.type!.colorData!.color!)
        whiteLabel = segment.type!.whiteLabel
        duration = "\(segment.activeHours ?? 0)h : \(segment.activeMinutes ?? 0)m"
        startTime = timeFormatter.timeString(from: segment.startDate) ?? "Unknown"
        endTime = timeFormatter.timeString(from: segment.endDate) ?? "Unknown"
    }
}

class PreviewCellViewModel: SegmentCellViewModeling {
    var title: String = "Preview Segment"
    var typeName: String
    var typeColor: Color
    var whiteLabel: Bool
    var duration: String = "1h : 30m"
    var startTime: String = "8:00 AM"
    var endTime: String = "9:30 AM"
    
    init(name: String, color: Color, whiteLabel: Bool) {
        typeName = name
        typeColor = color
        self.whiteLabel = whiteLabel
    }
}

