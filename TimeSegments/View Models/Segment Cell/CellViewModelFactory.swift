import CoreData

protocol SegmentCellViewModelCreating {
    associatedtype ViewModel: SegmentCellViewModeling
    
    func viewModel(for segEntity: SegmentEntity) -> ViewModel
}

struct SegmentCellViewModelFactory: SegmentCellViewModelCreating {
    func viewModel(for segEntity: SegmentEntity) -> SegmentCellViewModel {
        SegmentCellViewModel(segment: segEntity)
    }
}
