import SwiftUI

//MARK:- Interface
protocol StatsCalculating {
    func chartSlices(from segments: [SegmentEntity], includeNoRecords: Bool) -> [ChartSlice]
}

//MARK:- Common Functionality
protocol StatsCalculator {
    func periodMinutes(from segments: [SegmentEntity]) -> Double
    func chartSlicesStarter(from periodTotal: Double, and segmentsTotal: Double) -> [ChartSlice]
    func chartSlices(from segments: [SegmentEntity]) -> [ChartSlice]
}

//MARK:- Default Implementation
extension StatsCalculating {
    func chartSlices(from segments: [SegmentEntity], includeNoRecords: Bool) -> [ChartSlice] {
        let calc: StatsCalculator = includeNoRecords ? NoRecordsStatsCalculator() : RecordsOnlyStatsCalculator()
        return calc.chartSlices(from: segments)
    }
}

extension StatsCalculator {
    func periodMinutes(from segments: [SegmentEntity]) -> Double {
        segments.sumInMinutes()
    }
    
    func chartSlicesStarter(from periodTotal: Double, and segmentsTotal: Double) -> [ChartSlice] {
        return [ChartSlice]()
    }
    
    func chartSlices(from segments: [SegmentEntity]) -> [ChartSlice] {
        if segments.count == 0 {
            return [ChartSlice]()
        }
        let periodTime = periodMinutes(from: segments)
        let segmentsTotalTime = segments.sumInMinutes()
        var slices = chartSlicesStarter(from: periodTime, and: segmentsTotalTime)
        let grouped = Dictionary(grouping: segments) { $0.type }

        grouped.forEach { (type, segs) in
            let typeTotalTime = segs.sumInMinutes()
            let percentage = (typeTotalTime / periodTime).round(to: 2) * 100
            slices.append(
                ChartSlice(name: type!.name!, color: Color(type!.colorData!.color!), labelColor: type!.whiteLabel ? .white : .black, percentage: percentage)
            )
        }
        return slices
    }
}

//MARK:- Output Type
struct ChartSlice {
    let name: String
    let color: Color
    let labelColor: Color
    let percentage: Double
}
