import SwiftUI

struct CalcFactory: StatsCalculating {
    
}

struct RecordsOnlyStatsCalculator: StatsCalculator {
    
}

struct NoRecordsStatsCalculator: StatsCalculator {
    func periodMinutes(from segments: [SegmentEntity]) -> Double {
        let start = Calendar.current.startOfDay(for: segments.last!.startDate!)
        let end = Calendar.current.startOfDay(for: segments.first!.startDate!)
        return DateInterval(start: start, end: end).duration
    }
    
    func chartSlicesStarter(from periodTotal: Double, and segmentsTotal: Double) -> [ChartSlice] {
        let noRecordsPercentage = 100 * (periodTotal - segmentsTotal) / periodTotal
        return [ChartSlice(name: "No Records", color: .gray, labelColor: .black, percentage: noRecordsPercentage.round(to: 2))]
    }
}

//MARK:- Helper Extensions
extension Collection where Self.Element == SegmentEntity {
    func sumInMinutes() -> Double {
        self.map{ $0.activeDuration!.totalInMinutes }.reduce(0, +)
    }
}

extension Double {
    func round(to decimalPlaces: Int) -> Double {
        NSString(string: String(format: "%.\(decimalPlaces)f", self)).doubleValue
    }
}
