import Foundation

struct SegmentBarCalc: GroupedSegmentsProvider {
    private var groupedSegments:  [(key: DateComponents, value: [SegmentEntity])] = []
    private var referenceDuration: Double = 0
    
    init(allSegments: [SegmentEntity]) {
        if allSegments.count > 0 {
            groupedSegments = sortedTuples(grouping: allSegments, by: groupKey(from:))
            referenceDuration = groupedSegments.max { $0.value.sumInMinutes() < $1.value.sumInMinutes() }!.value.sumInMinutes()
        }
    }
    
    func calculations(for date: Date) -> (hours: Int, minutes: Int, percent: Double) {
        let currentDayGroup = groupedSegments.first {
            let gDate = Calendar.current.date(from: $0.key)!
            return Calendar.current.isDate(gDate, inSameDayAs: date)
        }
        if let group = currentDayGroup {
            let minutes = group.value.map { Int($0.activeDuration!.totalInMinutes) }.reduce(0, +)
            print(minutes)
            return (hours: minutes / 60,
                    minutes: minutes % 60,
                    (group.value.sumInMinutes() / referenceDuration)
            )
        }
        return (hours: 0, minutes: 0, percent: 0)
    }
}
