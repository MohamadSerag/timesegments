import CoreData
import UIKit.UIColor

protocol StoreManaging {
    var persistentContainer: NSPersistentCloudKitContainer { get }
    var viewContext: NSManagedObjectContext { get }
    
    func loadDefaults()
}

class CoreDataStoreManager: StoreManaging {
    static let shared = CoreDataStoreManager()
    
    var persistentContainer: NSPersistentCloudKitContainer
    var viewContext: NSManagedObjectContext { persistentContainer.viewContext }

    private var defaultDataLoaders: [DefaultDataLoading] { [SegmentTypeLoader(entityName: "CD_SegmentTypeEntity")] }
    
    init() {
        persistentContainer = NSPersistentCloudKitContainer(name: "TimeSegments")
        persistentContainer.loadPersistentStores { _, _ in
            try? self.persistentContainer.viewContext.setQueryGenerationFrom(.current)
            self.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
            self.persistentContainer.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        }
    }
    
    func loadDefaults() {
        defaultDataLoaders.forEach { $0.loadDefaultEntriesIfNeeded() }
    }
    
    func saveChanges() {
        if viewContext.hasChanges {
            try! viewContext.save()
        }
    }
}

//MARK:- Object Context Extension
extension NSManagedObjectContext {
    func segmentsSortedByStartDate() -> [SegmentEntity] {
        try! fetch(SegmentEntity.sortedFetchReq)
    }
    
    func firmDelete(_ obj: NSManagedObject) {
        delete(obj)
        try! save()
    }
}
