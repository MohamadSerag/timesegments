import CoreData
import CloudKit
import UIKit.UIColor

//MARK:- Interface
protocol DefaultDataLoading {
    func loadDefaultEntriesIfNeeded()
}

//MARK:- Common Functionality
protocol DefaultDataLoader: DefaultDataLoading {
    var entityName: String { get }
    var moc: NSManagedObjectContext { get  }
    var ckDatabase: CKDatabase { get }
    var inStore: Bool { get }
    
    func loadDefaultEntries()
    func checkCloudStore(onEmptyCompletion: @escaping () -> ())
    
}

//MARK:- Default Implementation
extension DefaultDataLoader {
    var moc: NSManagedObjectContext { CoreDataStoreManager.shared.viewContext }
    var ckDatabase: CKDatabase { CKContainer.default().privateCloudDatabase }
    
    func checkCloudStore(onEmptyCompletion: @escaping () -> ()) {
        let op = CKQueryOperation(query: CKQuery(recordType: entityName, predicate: NSPredicate(value: true)))
        op.zoneID = CKRecordZone(zoneName: "com.apple.coredata.cloudkit.zone").zoneID
        op.qualityOfService = .userInitiated
        op.resultsLimit = 1
        
        var fetched: CKRecord?
        op.recordFetchedBlock = { fetched = $0 }
        op.completionBlock = { if fetched == nil { onEmptyCompletion() } }
        ckDatabase.add(op)
    }
    
    func loadDefaultEntriesIfNeeded() {
        if inStore == false {
            checkCloudStore { DispatchQueue.main.async { self.loadDefaultEntries() } }
        }
    }
}

//MARK:- Concrete Objects
struct SegmentTypeLoader: DefaultDataLoader {
    var entityName: String
    var inStore: Bool { try! moc.count(for: SegmentTypeEntity.fetchRequest()) > 0 }
    
    func loadDefaultEntries() {
        let names = ["Exercise", "Work", "Charity", "Fun"]
        let colorData = [UIColor.blue.data, UIColor.red.data, UIColor.green.data, UIColor.yellow.data]
        let labelColor = [true, false, false, false]
        names.enumerated().forEach { (index, name) in
            let type = SegmentTypeEntity(context: moc)
            type.name = name
            type.colorData = colorData[index]
            type.whiteLabel = labelColor[index]
        }
        try! moc.save()
    }
}
