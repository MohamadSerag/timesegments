import CoreData

class SegmentTypeEntity: NSManagedObject, Identifiable, TypeInfoProviding {
    static var sortedFetchReq: NSFetchRequest<SegmentTypeEntity> {
        let req: NSFetchRequest<SegmentTypeEntity> = fetchRequest()
        req.sortDescriptors = [NSSortDescriptor(keyPath: \SegmentTypeEntity.name!, ascending: true)]
        return req
    }

    var segments: [SegmentEntity] { segmentSet!.map { $0 as! SegmentEntity } }
}

class SegmentEntity: NSManagedObject, Identifiable, SegmentInfoProviding {
    static var sortedFetchReq: NSFetchRequest<SegmentEntity> {
        let req: NSFetchRequest<SegmentEntity> = fetchRequest()
        req.sortDescriptors = [NSSortDescriptor(keyPath: \SegmentEntity.startDate!, ascending: false)]
        return req
    }
    static var todaySegmentsFetchReq: NSFetchRequest<SegmentEntity> {
        let bounds = Calendar.current.todayBounds()
        let req: NSFetchRequest<SegmentEntity> = SegmentEntity.fetchRequest()
        req.predicate = NSPredicate(format: "startDate >= %@ && startDate <= %@", argumentArray: [bounds.start, bounds.end])
        req.sortDescriptors = [NSSortDescriptor(keyPath: \SegmentEntity.startDate!, ascending: false)]
        return req
    }
    var activeHours: Int? { activeDuration?.hours }
    var activeMinutes: Int? { activeDuration?.minutes }
}

class SegmentDurationEntity: NSManagedObject {
    var hours: Int {
        get { Int(hours16) }
        set { hours16 = Int16(newValue) }
    }
    var minutes: Int {
        get { Int(minutes16) }
        set { minutes16 = Int16(newValue) }
    }
    var seconds: Int {
        get { Int(seconds16) }
        set { seconds16 = Int16(newValue) }
    }
    var totalInMinutes: Double {
        Double((hours * 60) + minutes)
    }
}

//MARK:- Helper Extensions
extension Calendar {
    func todayBounds() -> (start: Date, end: Date) {
        (start: startOfDay(for: Date()),
         end: startOfDay(for: date(byAdding: .day, value: 1, to: Date())!))
    }
}
