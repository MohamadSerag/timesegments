import CoreData

protocol SegmentExporting {
    func allToCSV() -> URL
}

struct SegmentExporter: SegmentExporting, MOCUser {
    var fetchReq: NSFetchRequest<SegmentEntity> {
        let req: NSFetchRequest<SegmentEntity> = SegmentEntity.fetchRequest()
        req.sortDescriptors = [
            NSSortDescriptor(keyPath: \SegmentEntity.startDate!, ascending: false), NSSortDescriptor(keyPath: \SegmentEntity.type!.name!, ascending: true)
        ]
        return req
    }
    
    func string(from seg: SegmentEntity) -> String {
        let startDate = DateFormatter.dayDateFormatter.string(from: seg.startDate!).replacingOccurrences(of: ",", with: "")
        let startTime = DateFormatter.timeDateFormatter.string(from: seg.startDate!)
        let endTime = DateFormatter.timeDateFormatter.string(from: seg.endDate!)
        return "\(seg.type!.name!),\(seg.title!),\(startDate),\(startTime),\(endTime),\(seg.activeDuration!.hours.description),\(seg.activeDuration!.minutes.description)"
    }
    
    func allToCSV() -> URL {
        var output = try! moc.fetch(fetchReq).map(string(from:))
        output.insert("Type Name,Seg. Title,Date,Start Time,End Time,Hours,Minutes", at: 0)
        
        let fileManager = FileManager.default
        let path = try! fileManager.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: false)
        let fileURL = path.appendingPathComponent("All Segments.csv")
        try! output.joined(separator: "\n").write(to: fileURL, atomically: true, encoding: .utf8)
        
        return fileURL
    }
}

/*
 Fetch all segments sorted by start date then type
 Create list segment string representation, with header at the first index
 Loop through segments converting each to a string representation:
 1. Type name
 2. Title
 3. Start Date
 4. Start Time
 5. End Time
 6. Duration
 Join with comma and append each to the list
 Join with "\n" and return
 */
