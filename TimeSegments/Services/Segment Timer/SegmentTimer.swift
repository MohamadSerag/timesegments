import SwiftUI
import Combine

class SegmentTimer {    
    //MARK:- Interface Properties
    @Published var startDate: Date?
    @Published var endDate: Date?
    @Published var activeDuration: DateComponents?
    var actionsInSequence: [() -> ()] { [start, pause, resume] }
    
    //MARK:- Dependencies
    internal var runloop = RunLoop.main
    internal var runloopMode = RunLoop.Mode.common
    internal var timer = Timer.self
    
    //MARK:- Private Properties
    private var durationCalculator: DurationCalculating
    private var tickInterval: TimeInterval!
    private var subs = [AnyCancellable]()
    private let timerSubIndex = 0
    private var state: State = .inactive

    init(durationCalculator: DurationCalculating = DurationCalculator(), tickInterval: TimeInterval = 1) {
        self.durationCalculator = durationCalculator
        self.tickInterval = tickInterval
    }
    
    //MARK:- Private Functions
    private func initTimePublisher() {
        timer.publish(every: tickInterval, on: runloop, in: runloopMode)
            .autoconnect()
            .compactMap {[weak self] date in
                guard let sureSelf = self else { return nil }
                return sureSelf.durationCalculator.activeDuration(between: sureSelf.startDate!, and: date)
        }
        .sink {[weak self] in self?.activeDuration = $0}
        .store(in: &subs)
    }
    
    private func resetOnStart() {
        endDate = nil
        activeDuration = nil
        durationCalculator.resetPauseSpans()
    }
    
    //MARK:- Interface Functions
    func start() {
        resetOnStart()
        
        initTimePublisher()
        startDate = Date()
        state = .active
    }
    
    func pause() {
        durationCalculator.pause()
        subs.remove(at: timerSubIndex).cancel()
        state = .paused
    }
    
    func resume() {
        durationCalculator.resume()
        initTimePublisher()
        state = .active
    }
    
    func end() {
        endDate = Date()
        subs.removeAll(keepingCapacity: true)
        state = .inactive
    }
}

extension SegmentTimer {
    enum State { case inactive, active, paused }
}
