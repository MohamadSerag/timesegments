import Foundation

protocol DurationCalculating {
    func pause()
    func resume()
    func activeDuration(between startDate: Date, and endDate: Date) -> DateComponents
    func resetPauseSpans()
}

class DurationCalculator: DurationCalculating {
    private var pauseDate: Date?
    private var pauseSpans = [DateComponents]()
    internal var calendar: Calendar { Calendar.current }
    
    func pause() {
        pauseDate = Date()
    }
    
    func resume() {
        pauseSpans.append(calendar.dateComponents([.day, .hour, .minute, .second], from: pauseDate!, to: Date()))
    }
    
    func activeDuration(between startDate: Date, and endDate: Date) -> DateComponents {
        var removedPausesEndDate = endDate
        pauseSpans.forEach { pauseSpan in
            removedPausesEndDate = calendar.date(byAdding: .second, value: -pauseSpan.second!, to: removedPausesEndDate)!
            removedPausesEndDate = calendar.date(byAdding: .minute, value: -pauseSpan.minute!, to: removedPausesEndDate)!
            removedPausesEndDate = calendar.date(byAdding: .hour, value: -pauseSpan.hour!, to: removedPausesEndDate)!
        }
        return calendar.dateComponents([.day, .hour, .minute, .second], from: startDate, to: removedPausesEndDate)
    }
    
    func resetPauseSpans() {
        pauseDate = nil
        pauseSpans = [DateComponents]()
    }
}
