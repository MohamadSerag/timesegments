import SwiftUI

struct PieChart: View {
    let chef: PieRecipeProviding
    
    var body: some View {
        GeometryReader { gr in
            ZStack {
                ForEach(self.chef.sliceRecipes(in: gr.frame(in: .local)), id: \.startAngle) {recipe in
                    PieSlice(recipe: recipe)
                }
            }
        }
    }
}

struct PieSlice: View {
    let recipe: SliceRecipe
    
    @Environment(\.colorScheme) var colorScheme
    var strokeColor: Color { colorScheme == .light ? .white : .black }
    
    private var path: Path {
        var path = Path()
        
        path.addArc(
            center: recipe.centerPoint,
            radius: recipe.radius,
            startAngle: .degrees(recipe.startAngle),
            endAngle: .degrees(recipe.endAngle),
            clockwise: recipe.clockwise
        )
        path.addLine(to: recipe.centerPoint)
        path.closeSubpath()
        return path
    }
    
    var body: some View {
        ZStack {
            path
                .fill()
                .foregroundColor(recipe.color)
                .overlay(path.stroke(strokeColor, lineWidth: 3))
            
            VStack {
                Text(recipe.title)
                    .fontWeight(.bold)
                Text("\(recipe.percentage, specifier: "%g")%")
                    .fontWeight(.bold)
            }
            .foregroundColor(recipe.labelColor)
            .offset(recipe.labelOffset)
        }
    }
}
