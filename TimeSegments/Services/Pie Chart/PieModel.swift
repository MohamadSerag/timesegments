import SwiftUI

protocol PieRecipeProviding {
    func sliceRecipes(in frame: CGRect) -> [SliceRecipe]
}

struct PieChef: PieRecipeProviding {
    let slices: [ChartSlice]
    let labelOffsetCalc: LabelOffsetCalculating = LabelOffsetCalculator()
    
    func sliceRecipes(in frame: CGRect) -> [SliceRecipe] {
        let centerPoint = CGPoint(x: frame.midX, y: frame.midY)
        let radius = min(frame.width, frame.height) / 2
        var recipes = [SliceRecipe]()
        var tempEndAngle = 0.0
        
        slices.sorted { $0.percentage > $1.percentage }.forEach { slice in
            let startAngle = tempEndAngle
            let endAngle = startAngle + (slice.percentage * 360 / 100)
            let labelOffset = labelOffsetCalc.offsetFor(
                startAngle: startAngle, endAngle: endAngle, radius: Double(radius * 0.6)
            )
            recipes.append(
                SliceRecipe(
                    startAngle: (startAngle - 90),
                    endAngle: (endAngle - 90),
                    labelOffset: labelOffset,
                    clockwise: false,
                    centerPoint: centerPoint,
                    radius: radius,
                    color: slice.color,
                    labelColor: slice.labelColor,
                    percentage: slice.percentage,
                    title: slice.name
                )
            )
            tempEndAngle = endAngle
        }
        return recipes
    }
}

//MARK:- Input/Output Definition
struct SliceRecipe {
    let startAngle: Double
    let endAngle: Double
    let labelOffset: CGSize
    let clockwise: Bool
    let centerPoint: CGPoint
    let radius: CGFloat
    let color: Color
    let labelColor: Color
    let percentage: Double
    let title: String
}
