import SwiftUI

protocol LabelOffsetCalculating {
    func offsetFor(startAngle: Double, endAngle: Double, radius: Double) -> CGSize
}

struct LabelOffsetCalculator: LabelOffsetCalculating {
    func offsetFor(startAngle: Double, endAngle: Double, radius: Double) -> CGSize {
        let ppAngle = ((endAngle - startAngle) / 2) + startAngle
        var xOffset = 0.0
        var yOffset = 0.0
        
        if ppAngle <= 90 {
            let alpha = 90 - ppAngle
            xOffset = cos(alpha * Double.pi / 180) * radius
            yOffset = -(sin(alpha * Double.pi / 180) * radius)
            
        } else if (ppAngle > 90) && (ppAngle <= 180) {
            let alpha = 180 - ppAngle
            xOffset = sin(alpha * Double.pi / 180) * radius
            yOffset = cos(alpha * Double.pi / 180) * radius
            
        } else if (ppAngle > 180) && (ppAngle <= 270) {
            let alpha = 270 - ppAngle
            xOffset = -(cos(alpha * Double.pi / 180) * radius)
            yOffset = sin(alpha * Double.pi / 180) * radius
            
        } else if ppAngle > 270 {
            let alpha = 360 - ppAngle
            xOffset = -(sin(alpha * Double.pi / 180) * radius)
            yOffset = -(cos(alpha * Double.pi / 180) * radius)
        }
        return CGSize(width: xOffset, height: yOffset)
    }
}


